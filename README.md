# HTTP Notebook Kernel for VSCode

A notebook extension that provides a kernel for making HTTP requests.

![apple 301](images/apple-com-301.png)

## Compatible formats

Currently, the kernel is only registered for Markdown files (`.md`,
`.markdown`). Theoretically, it should work with any format.