import * as vscode from 'vscode'
import { Kernel } from './kernel'

export function activate(context: vscode.ExtensionContext) {
	const kernel = new KernelProvider()

	context.subscriptions.push(vscode.notebook.registerNotebookKernelProvider({
		viewType: ['http'],
		filenamePattern: '*.{md,markdown}',
	}, kernel))
}

// this method is called when your extension is deactivated
export function deactivate() {}

class KernelProvider implements vscode.NotebookKernelProvider<Kernel> {
    provideKernels(document: vscode.NotebookDocument, token: vscode.CancellationToken): Kernel[] {
        return [new Kernel]
    }
}