import vscode = require('vscode')
import http = require('http')
import https = require('https')

export class Kernel implements vscode.NotebookKernel {
    id = 'http-kernel'
    label = 'HTTP Request Kernel'
    description = 'Kernel for executing HTTP requests'
    supportedLanguages = ['http']

    private methods = new Set(['GET', 'PUT', 'DELETE', 'POST', 'HEAD', 'OPTIONS', 'PATCH'])
    private schemes = { http, https }
    private diagnostics = vscode.languages.createDiagnosticCollection()

    async executeCellsRequest(document: vscode.NotebookDocument, ranges: vscode.NotebookCellRange[]): Promise<void> {
        await Promise.all(ranges.flatMap(r =>
            Array.from({ length: r.end - r.start }).map(async (_, i) => {
                const task = vscode.notebook.createNotebookCellExecutionTask(document.uri, i + r.start, this.id)!

                task.start()
                task.clearOutput()
                try {
                    const result = await this.executeCell(task)
                    task.end(result)
                } catch (error) {
                    vscode.window.showErrorMessage(error.message)
                    task.end({ success: false })
                }
            })
        ))
    }

    async executeCell(task: vscode.NotebookCellExecutionTask): Promise<vscode.NotebookCellExecuteEndContext | undefined> {
        let text = task.cell.document.getText()
        text = text.replace(/^--.*\n/gm, '').replace(/-- .*/g, '')

        const lines = text.split('\n')
        if (lines.length == 0) {
            this.appendOutput(task, { 'application/x.notebook.stderr': 'Request is empty!' })
            return { success: false }
        }

        let { groups } = lines[0].match(/^(?<method>\w+)(?<ws>\s+)(?<url>.+)$/) || {}
        if (!groups) {
            this.setDiagnostics(task, new vscode.Diagnostic(new vscode.Range(0, 0, 0, lines[0].length - 1), 'Expected `<method> <url>', vscode.DiagnosticSeverity.Error))
            this.appendOutput(task, { 'application/x.notebook.stderr': 'Invalid request' })
            return { success: false }
        }

        const { method } = groups
        if (!this.methods.has(method.toUpperCase())) {
            this.setDiagnostics(task, new vscode.Diagnostic(new vscode.Range(0, 0, 0, method.length - 1), 'Unsupported method', vscode.DiagnosticSeverity.Error))
            this.appendOutput(task, { 'application/x.notebook.stderr': `Unsupported method '${method}'` })
            return { success: false }
        }

        let uri: vscode.Uri
        try {
            uri = vscode.Uri.parse(groups.url)
        } catch (error) {
            const start = groups.method.length + groups.ws.length
            this.setDiagnostics(task, new vscode.Diagnostic(new vscode.Range(0, start, 0, lines[0].length - 1), error.message, vscode.DiagnosticSeverity.Error))
            this.appendOutput(task, { 'application/x.notebook.stderr': error.message })
            return { success: false }
        }

        if (!(uri.scheme.toLowerCase() in this.schemes)) {
            const start = groups.method.length + groups.ws.length
            this.setDiagnostics(task, new vscode.Diagnostic(new vscode.Range(0, start, 0, start + uri.scheme.length), 'Unsupported scheme', vscode.DiagnosticSeverity.Error))
            this.appendOutput(task, { 'application/x.notebook.stderr': `Unsupported scheme '${method}'` })
            return { success: false }
        }

        const headers: Record<string, string | string[]> = {
            host: uri.authority,
        }

        let i
        for (i = 1; i < lines.length; i++) {
            const line = lines[i]
            if (line.trim().length == 0)
                break;

            let [name, value] = line.split(/:\s*/, 2)
            if (!value) {
                this.setDiagnostics(task, new vscode.Diagnostic(new vscode.Range(i, 0, i, line.length), 'Invalid header', vscode.DiagnosticSeverity.Error))
                this.appendOutput(task, { 'application/x.notebook.stderr': `'${line}' is not a valid HTTP header` })
                return { success: false }
            }

            name = name.toLowerCase()
            if (!(name in headers) || name == 'host')
                headers[name] = value
            else if (headers[name] instanceof Array)
                (<string[]>headers[name]).push(value)
            else
                headers[name] = [<string>headers[name], value]
        }
        i++

        let r
        try {
            r = await this.request(uri, { headers })
        } catch (error) {
            this.appendOutput(task, { 'application/x.notebook.stderr': error.message })
            return
        }
        const { resp, body } = r

        this.appendOutput(task, {
            'text/plain': `${resp.statusCode} ${resp.statusMessage}\n${resp.rawHeaders.reduce((acc, v, i) => i % 2 ? `${acc}: ${v}` : acc ? `${acc}\n${v}` : v)}`
        })

        const ct = resp.headers['content-type']
        if (!ct) {
            this.appendOutput(task, {
                'text/plain': body.toString(),
            })
            return
        }
        
        groups = ct.match(/^(?<mime>.*); charset=(?<charset>.*)$/)?.groups
        if (groups) {
            const str = body.toString(<BufferEncoding>groups.charset.toLowerCase())
            this.appendOutput(task, {
                [groups.mime]: checkJson(groups.mime, str),
                'text/plain': str,
            })
            return
        }

        const str = body.toString()
        this.appendOutput(task, {
            [ct]: checkJson(ct, str),
            'text/plain': str,
        })

        function checkJson(mime: string, str: string) {
            if (mime == 'application/json' || mime == 'text/json')
                try {
                    return JSON.parse(str)
                } catch (e) {}
            
            return str
        }
    }

    private setDiagnostics(task: vscode.NotebookCellExecutionTask, diagnostics: vscode.Diagnostic | vscode.Diagnostic[]) {
        if (diagnostics instanceof vscode.Diagnostic)
            diagnostics = [diagnostics]

        this.diagnostics.set(task.cell.document.uri, diagnostics)
    }

    private appendOutput(task: vscode.NotebookCellExecutionTask, content: Record<string, string>) {
        const items = Object.entries(content).map(([mime, value]) => new vscode.NotebookCellOutputItem(mime, value))
        task.appendOutput([new vscode.NotebookCellOutput(items)])
    }

    private async request(uri: vscode.Uri, options: http.RequestOptions, requestBody?: any) {
        const resp = await new Promise<http.IncomingMessage>((resolve, reject) => {
            const req = this.schemes[<'http'|'https'>uri.scheme].request(uri.toString(), options, resolve)
            req.on('error', reject)
            if (requestBody) req.write(requestBody)
            req.end()
        })

        const body = await new Promise<Buffer>(resolve => {
            const body: Buffer[] = []
            resp.on('data', c => body.push(c))
            resp.on('end', () => resolve(Buffer.concat(body)))
        })
        
        return { resp, body }
    }
}